#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset

SLS_DEPLOYMENTS=$(find api/* -name \*serverless.yml)

for sls in $SLS_DEPLOYMENTS
do
    group=$(echo $sls | cut -d'/' -f2)
    cd api/$group
    echo "🤖  Deploying: [$group]..."
    serverless deploy --conceal --stage $env_stage
    echo "🤡  Finished Deploying: [$group]..."
    cd ../../
done