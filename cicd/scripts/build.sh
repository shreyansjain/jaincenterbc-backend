#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset

APIS=$(find api/* -name \*main.go)

for api in $APIS
do
    group=$(echo $api | cut -d'/' -f2)
    apiName=$(echo $api | cut -d'/' -f3)
    echo "🤖  Building: [$apiName]..."
    mkdir -p $PROJECT_DIR/api/$group/bin
    env GOARCH=amd64 GOOS=linux go build -mod=vendor -o $PROJECT_DIR/api/$group/bin/$apiName -ldflags="-s -w" $PROJECT_DIR/api/$group/$apiName
done