module gitlab.com/jaincenterbc

go 1.16

require (
	github.com/aws/aws-lambda-go v1.27.0
	github.com/golang-migrate/migrate/v4 v4.15.1
	github.com/google/uuid v1.3.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.10.3
	github.com/onsi/ginkgo v1.12.1
	github.com/onsi/gomega v1.10.3
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	syreclabs.com/go/faker v1.2.3
)
