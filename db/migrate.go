package main

import (
	"database/sql"
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq" // postgres driver
	"github.com/pkg/errors"
	"gitlab.com/jaincenterbc/application/utils"
)

func main() {

	// Read secrets from the env file
	utils.ReadSecrets()

	var (
		EnvStage   = utils.GetEnv("env_stage", "local")
		DbHost     = utils.GetEnv("DB_HOST", "host.docker.internal")
		DbPort     = utils.GetEnv("DB_PORT", "5433")
		DbName     = utils.GetEnv("DB_NAME", "local_jaincenterbc")
		DbUser     = utils.GetEnv("DB_USER", "postgres")
		DbPassword = utils.GetEnv("DB_PASSWORD", "Password1")
	)

	// TODO: sslMode preferably should be verify-full
	// For that, we need the cert for that.
	sslMode := "require"
	if EnvStage == "local" {
		sslMode = "disable"
	}

	// Connect to DB
	dbConnStr := fmt.Sprintf("host=%s port=%s dbname=%s user=%s password=%s sslmode=%s",
		DbHost, DbPort, DbName, DbUser, DbPassword, sslMode)

	db, err := sql.Open("postgres", dbConnStr)
	if err != nil {
		errors.Wrap(err, "Failed to connect to the db")
		panic(err)
	}

	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		errors.Wrap(err, "Failed to initialize postgres instance")
		panic(err)
	}

	projectDirectory := utils.GetEnv("PROJECT_DIR", "")
	m, err := migrate.NewWithDatabaseInstance(
		fmt.Sprintf("file:///%s/db/migrations", projectDirectory),
		"postgres", driver)
	if err != nil {
		errors.Wrap(err, "Failed to initialize migration directory")
	}

	activeVersion, dirty, err := m.Version()
	if err != nil {
		e := errors.Cause(err)
		// Useful when DB is clean (no migration)
		if e.Error() != "no migration" {
			panic(err)
		}
	}

	if dirty {
		panic(fmt.Sprintf("version: [%d] is dirty", activeVersion))
	}

	// Migrate to following version if active version is not the same as version to migrate to
	migrationVersionNumber := 6
	if int(activeVersion) == migrationVersionNumber {
		return
	}

	err = m.Up()
	if err != nil {
		errors.Wrap(err, fmt.Sprintf("Failed to migrate to version %d", migrationVersionNumber))
		panic(err)
	}
	fmt.Printf("Successfully migrated to version: [%d]\n\n", migrationVersionNumber)
}
