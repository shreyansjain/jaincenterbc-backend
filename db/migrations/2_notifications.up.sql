BEGIN;

-- User Notifications

CREATE TABLE
IF NOT EXISTS user_notifications
(
    id SERIAL,
    cognitousername character(36) NOT NULL,
    title CHARACTER VARYING,
    message CHARACTER VARYING,
    created timestamp without time zone NOT NULL DEFAULT NOW(),
    modified timestamp without time zone NOT NULL DEFAULT NOW(),
    createdby CHARACTER VARYING NOT NULL DEFAULT CURRENT_USER,
    modifiedby CHARACTER VARYING NOT NULL DEFAULT CURRENT_USER,
    CONSTRAINT usernotif_pkey PRIMARY KEY (id),
    CONSTRAINT usernotif_user_fkey FOREIGN KEY (cognitousername) REFERENCES public.users (cognitousername) MATCH SIMPLE ON DELETE CASCADE
);

COMMIT;