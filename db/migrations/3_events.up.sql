BEGIN;

-- Events

CREATE TABLE
IF NOT EXISTS events
(
    id SERIAL,
    title CHARACTER VARYING,
    description CHARACTER VARYING,
    weburl CHARACTER VARYING,
    datetime timestamp without time zone NOT NULL,
    created timestamp without time zone NOT NULL DEFAULT NOW(),
    modified timestamp without time zone NOT NULL DEFAULT NOW(),
    createdby CHARACTER VARYING NOT NULL DEFAULT CURRENT_USER,
    modifiedby CHARACTER VARYING NOT NULL DEFAULT CURRENT_USER,
    CONSTRAINT events_pkey PRIMARY KEY (id)
);

COMMIT;