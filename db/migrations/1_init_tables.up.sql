BEGIN;

-- User Information

CREATE TABLE
IF NOT EXISTS users
(
    id SERIAL,
    cognitousername character(36) NOT NULL,
    firstname CHARACTER VARYING NOT NULL,
    lastname CHARACTER VARYING NOT NULL,
    email CHARACTER VARYING,
    telephone CHARACTER VARYING,
    member BOOLEAN DEFAULT FALSE,
    created timestamp without time zone NOT NULL DEFAULT NOW(),
    modified timestamp without time zone NOT NULL DEFAULT NOW(),
    createdby character varying NOT NULL DEFAULT CURRENT_USER,
    modifiedby character varying NOT NULL DEFAULT CURRENT_USER,
    UNIQUE(cognitousername),
    CONSTRAINT user_pkey PRIMARY KEY (cognitousername)
);

COMMIT;