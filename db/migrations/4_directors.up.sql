BEGIN;

-- Board of Directors Information

CREATE TABLE
IF NOT EXISTS directors
(
    id SERIAL,
    firstname CHARACTER VARYING NOT NULL,
    lastname CHARACTER VARYING NOT NULL,
    email CHARACTER VARYING NOT NULL,
    telephone CHARACTER VARYING NOT NULL,
    position CHARACTER VARYING NOT NULL,
    active BOOLEAN DEFAULT TRUE,
    created timestamp without time zone NOT NULL DEFAULT NOW(),
    modified timestamp without time zone NOT NULL DEFAULT NOW(),
    createdby character varying NOT NULL DEFAULT CURRENT_USER,
    modifiedby character varying NOT NULL DEFAULT CURRENT_USER,
    CONSTRAINT directors_pkey PRIMARY KEY (id)
);

COMMIT;