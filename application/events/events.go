package events

import (
	"database/sql"

	"github.com/pkg/errors"
)

// Event struct
type Event struct {
	ID          *int   `json:"id,omitempty"`
	Title       string `json:"title,omitempty"`
	Description string `json:"description,omitempty"`
	WebLink     string `json:"web_url,omitempty"`
	DateTime    string `json:"timestamp,omitempty"`
}

// GetEvents fetches all the events
func GetEvents(dbClient *sql.DB) ([]*Event, error) {

	sqlStatement := `
						SELECT		id, title, description, COALESCE(weburl,''), datetime
						FROM 		events
						ORDER BY	id DESC
						LIMIT		50;
					`

	rows, err := dbClient.Query(sqlStatement)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to get all events")
	}
	defer rows.Close()

	events := make([]*Event, 0)
	for rows.Next() {
		evt := new(Event)
		err = rows.Scan(&evt.ID, &evt.Title, &evt.Description, &evt.WebLink, &evt.DateTime)
		if err != nil {
			return nil, errors.Wrap(err, "Failed to scan events row")
		}

		events = append(events, evt)
	}

	return events, nil
}
