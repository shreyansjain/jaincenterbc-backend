package events_test

import (
	"math/rand"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/jaincenterbc/application/events"
)

var _ = Describe("Events", func() {
	Context("Test application/events", func() {
		It("should successfully GetNotifications()", func() {
			events, err := GetEvents(dbClient)
			Expect(err).To(BeNil())

			if len(events) > 0 {
				random := 0
				if len(events) > 1 {
					random = rand.Intn(len(events) - 1)
				}

				Expect(*events[random].ID).Should(BeNumerically(">", 0))
				Expect(events[random].Title).NotTo(BeNil())
				Expect(events[random].Description).NotTo(BeNil())
				Expect(events[random].DateTime).NotTo(BeNil())
			}
		})
	})
})
