package organization_test

import (
	"math/rand"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/jaincenterbc/application/organization"
)

var _ = Describe("Organization", func() {
	Context("Test application/organization", func() {
		It("should successfully GetBoardDirectors()", func() {
			directors, err := GetBoardDirectors(dbClient)
			Expect(err).To(BeNil())

			if len(directors) > 0 {
				random := 0
				if len(directors) > 1 {
					random = rand.Intn(len(directors) - 1)
				}

				Expect(*directors[random].ID).Should(BeNumerically(">", 0))
				Expect(directors[random].FirstName).NotTo(BeNil())
				Expect(directors[random].LastName).NotTo(BeNil())
				Expect(directors[random].Email).NotTo(BeNil())
				Expect(directors[random].PhoneNumber).NotTo(BeNil())
				Expect(directors[random].Position).NotTo(BeNil())
			}
		})
	})
})
