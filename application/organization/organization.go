package organization

import (
	"database/sql"

	"github.com/pkg/errors"
)

// BoardOfDirector struct
type BoardOfDirector struct {
	ID          *int   `json:"id,omitempty"`
	Salutation  string `json:"salutation,omitempty"`
	FirstName   string `json:"first_name,omitempty"`
	LastName    string `json:"last_name,omitempty"`
	Email       string `json:"email,omitempty"`
	PhoneNumber string `json:"phone_number,omitempty"`
	Position    string `json:"position,omitempty"`
}

// GetBoardDirectors fetches all active board of directors
func GetBoardDirectors(dbClient *sql.DB) ([]*BoardOfDirector, error) {

	sqlStatement := `
						SELECT		id, COALESCE(salutation, ''), firstname, lastname, COALESCE(email, ''), COALESCE(telephone, ''), position
						FROM 		directors
						WHERE		active = TRUE;
					`

	rows, err := dbClient.Query(sqlStatement)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to get all events")
	}
	defer rows.Close()

	directors := make([]*BoardOfDirector, 0)
	for rows.Next() {
		bod := new(BoardOfDirector)
		err = rows.Scan(&bod.ID, &bod.Salutation, &bod.FirstName, &bod.LastName, &bod.Email, &bod.PhoneNumber, &bod.Position)
		if err != nil {
			return nil, errors.Wrap(err, "Failed to scan directors row")
		}

		directors = append(directors, bod)
	}

	return directors, nil
}
