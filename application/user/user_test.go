package user_test

import (
	"math/rand"

	"github.com/google/uuid"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"syreclabs.com/go/faker"

	. "gitlab.com/jaincenterbc/application/user"
)

var _ = Describe("User", func() {
	Context("Test application/user", func() {
		var testUser *User

		It("should successfully CREATE a new user", func() {
			txn, err := dbClient.Begin()
			Expect(err).To(BeNil())

			usr := &User{
				CognitoUserName: uuid.New().String(),
				FirstName:       faker.Name().FirstName(),
				LastName:        faker.Name().LastName(),
				PhoneNumber:     faker.PhoneNumber().CellPhone(),
			}

			err = usr.Create(txn)
			Expect(err).To(BeNil())
			Expect(usr.ID).NotTo(BeNil())
			Expect(*usr.ID).Should(BeNumerically(">", 0))

			err = txn.Commit()
			Expect(err).To(BeNil())

			testUser = usr
		})

		It("should successfully GET a User by ID", func() {
			c := new(User)
			c.ID = testUser.ID

			err := c.Get(dbClient)
			Expect(err).To(BeNil())
			Expect(*c.ID).Should(BeNumerically(">", 0))
			Expect(c.CognitoUserName).Should(Equal(testUser.CognitoUserName))
			Expect(c.FirstName).Should(Equal(testUser.FirstName))
			Expect(c.LastName).Should(Equal(testUser.LastName))
			Expect(c.PhoneNumber).Should(Equal(testUser.PhoneNumber))
		})

		It("should successfully GET a User by cognito username", func() {
			c := new(User)
			c.CognitoUserName = testUser.CognitoUserName

			err := c.Get(dbClient)
			Expect(err).To(BeNil())
			Expect(*c.ID).Should(BeNumerically(">", 0))
			Expect(c.FirstName).Should(Equal(testUser.FirstName))
			Expect(c.LastName).Should(Equal(testUser.LastName))
			Expect(c.PhoneNumber).Should(Equal(testUser.PhoneNumber))
		})

		It("should successfully PATCH a User by cognito username", func() {
			c := new(User)
			c.CognitoUserName = testUser.CognitoUserName
			c.FirstName = faker.Name().FirstName()
			c.Email = faker.Internet().Email()

			txn, err := dbClient.Begin()
			Expect(err).To(BeNil())

			err = c.Patch(txn)
			Expect(err).To(BeNil())

			err = txn.Commit()
			Expect(err).To(BeNil())
		})

		It("should successfully GetNotifications()", func() {
			u := &User{
				CognitoUserName: testUser.CognitoUserName,
			}
			notifs, err := u.GetNotifications(dbClient)
			Expect(err).To(BeNil())

			if len(notifs) > 0 {
				random := 0
				if len(notifs) > 1 {
					random = rand.Intn(len(notifs) - 1)
				}

				Expect(*notifs[random].ID).Should(BeNumerically(">", 0))
				Expect(notifs[random].Title).NotTo(BeNil())
				Expect(notifs[random].Message).NotTo(BeNil())
				Expect(notifs[random].Created).NotTo(BeNil())
			}
		})
	})
})
