package user

import (
	"database/sql"

	"github.com/pkg/errors"
)

// Notification ...
type Notification struct {
	CognitoUserName string   `json:"cognito_username"`
	ID              *int     `json:"id,omitempty"`
	Key             string   `json:"key,omitempty"`
	Title           string   `json:"title,omitempty"`
	Message         string   `json:"message,omitempty"`
	Recipients      []string `json:"recipients,omitempty"`
	Created         string   `json:"created,omitempty"`
}

// GetNotifications fetches the User Notifications using cognito_username as the key
func (u *User) GetNotifications(dbClient *sql.DB) ([]*Notification, error) {
	if len(u.CognitoUserName) == 0 {
		return nil, errors.New("Cognito Username cannot be null")
	}

	sqlStatement := `
						SELECT		id, title, message
						FROM 		user_notifications
						WHERE		cognitousername = $1
						ORDER BY	id DESC
						LIMIT		50;
					`

	rows, err := dbClient.Query(sqlStatement, u.CognitoUserName)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to get all user notifications")
	}
	defer rows.Close()

	userNotifs := make([]*Notification, 0)
	for rows.Next() {
		cn := new(Notification)

		err = rows.Scan(&cn.ID, &cn.Title, &cn.Message, &cn.Created)
		if err != nil {
			return nil, errors.Wrap(err, "Failed to scan user notification row")
		}

		userNotifs = append(userNotifs, cn)
	}

	return userNotifs, nil
}
