package user

import (
	"database/sql"
	"fmt"
	"strings"

	"github.com/pkg/errors"
)

// User struct
type User struct {
	CognitoUserName string `json:"cognito_username"`
	ID              *int   `json:"id,omitempty"`
	FirstName       string `json:"first_name,omitempty"`
	LastName        string `json:"last_name,omitempty"`
	Email           string `json:"email,omitempty"`
	PhoneNumber     string `json:"phone_number,omitempty"`
	IsMember        *bool  `json:"member,omitempty"`
}

// Create a new User
func (u *User) Create(dbTxn *sql.Tx) error {
	if len(u.CognitoUserName) == 0 {
		return errors.New("Cognito Username cannot be null")
	}
	if len(u.FirstName) == 0 {
		return errors.New("First Name cannot be null")
	}
	if len(u.LastName) == 0 {
		return errors.New("Last Name cannot be null")
	}
	if len(u.PhoneNumber) == 0 {
		return errors.New("Phone Number cannot be null")
	}
	if len(u.Email) == 0 {
		u.Email = ""
	}

	sqlStatement := `
						INSERT INTO users (cognitousername, firstname, lastname,
											email, telephone)
						VALUES ($1, $2, $3, $4, $5)
						RETURNING id;
					`

	err := dbTxn.QueryRow(sqlStatement, u.CognitoUserName, u.FirstName, u.LastName,
		u.Email, u.PhoneNumber).Scan(&u.ID)
	if err != nil {
		return errors.Wrap(err, "Failed to create new user")
	}

	return nil
}

// Get fetches the User info using using ID or cognito_username as the key
func (u *User) Get(dbClient *sql.DB) error {
	var args []interface{}
	sqlStatement := `
						SELECT	id, cognitousername, firstname, lastname,
								telephone, COALESCE(email,''), member
						FROM 	users
					`

	if u.ID != nil {
		sqlStatement += `
						WHERE	id = $1;
		`

		args = append(args, *u.ID)
	} else if len(u.CognitoUserName) != 0 {
		sqlStatement += `
						WHERE	cognitousername = $1;
		`

		args = append(args, u.CognitoUserName)
	} else {
		return errors.New("User ID or cognito:username cannot be null")
	}

	err := dbClient.QueryRow(sqlStatement, args...).Scan(&u.ID, &u.CognitoUserName, &u.FirstName,
		&u.LastName, &u.PhoneNumber, &u.Email, &u.IsMember)
	if err != nil {
		return errors.Wrapf(err, "Failed to get user for %s", args...)
	}

	return nil
}

// Patch updates the User using cognito_username as the key
func (u *User) Patch(dbTxn *sql.Tx) error {
	if len(u.CognitoUserName) == 0 {
		return errors.New("Cognito Username cannot be null")
	}

	//Make sure something is to validate
	if len(u.FirstName) == 0 && len(u.LastName) == 0 && len(u.Email) == 0 {
		return errors.New("Something needs to be updated")
	}

	var args []interface{}
	sqlStatement := `
						UPDATE	users SET
					`

	val := 1
	if len(u.FirstName) != 0 {
		sqlStatement += fmt.Sprintf(" firstname = $%d,", val)
		args = append(args, u.FirstName)
		val++
	}
	if len(u.LastName) != 0 {
		sqlStatement += fmt.Sprintf(" lastname = $%d,", val)
		args = append(args, u.LastName)
		val++
	}
	if len(u.Email) != 0 {
		sqlStatement += fmt.Sprintf(" email = $%d,", val)
		args = append(args, u.Email)
		val++
	}

	sqlStatement = strings.TrimSuffix(sqlStatement, ",")
	sqlStatement += fmt.Sprintf(" WHERE	cognitousername = $%d;", val)
	args = append(args, u.CognitoUserName)

	_, err := dbTxn.Exec(sqlStatement, args...)
	if err != nil {
		return errors.Wrapf(err, "Failed to patch user for %s", u.CognitoUserName)
	}
	return nil
}
