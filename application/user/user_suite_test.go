package user_test

import (
	"database/sql"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/jaincenterbc/application/utils"
	pg "gitlab.com/jaincenterbc/application/utils/db"
)

var dbClient *sql.DB

func TestUnit(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "User Suite")
}

var _ = BeforeSuite(func() {
	// For reading the secrets in environment variables
	utils.ReadSecrets()

	db, err := pg.CreateConnection()
	dbClient = db // assign to global var
	Expect(err).NotTo(HaveOccurred())
	Expect(dbClient.Ping()).To(BeNil())
})

var _ = AfterSuite(func() {
	pg.CloseConnection(dbClient)
})
