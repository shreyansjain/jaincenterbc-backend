package utils

import (
	"database/sql"
	"encoding/json"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/sirupsen/logrus"
	"gitlab.com/jaincenterbc/application/utils/logging"
)

// EnvironmentStage ...
type EnvironmentStage string

// Environment stages
const (
	EnvironmentStageLocal EnvironmentStage = "local"
	EnvironmentStageTest  EnvironmentStage = "test"
	EnvironmentStageDev   EnvironmentStage = "dev"
	EnvironmentStageProd  EnvironmentStage = "prod"
)

// APIError holds information for error object used in error response
type APIError struct {
	Message string `json:"message,omitempty"`
}

// AuthorizerClaims structifies APIGateway RequestContext Authorizer Claim
type AuthorizerClaims struct {
	CognitoUserName string `json:"cognito:username" binding:"required"`
	PhoneNumber     string `json:"phone_number" binding:"required"`
	FirstName       string `json:"given_name" binding:"required"`
}

// Context holds the key information
type Context struct {
	Log        *logrus.Entry
	Authorizer *AuthorizerClaims
	DB         *sql.DB
}

// InitContext initializes the context struct
func InitContext(req events.APIGatewayProxyRequest, log *logging.Logger, dbClient *sql.DB) *Context {
	claims := req.RequestContext.Authorizer["claims"].(map[string]interface{})
	cognitoUsername := claims["cognito:username"].(string)
	phoneNumber := claims["phone_number"].(string)

	// Set fields to logrus client
	log.Logrus = log.Logrus.WithFields(logrus.Fields{"cognito_username": cognitoUsername})

	// Setup Sentry
	// debug := true
	env := GetEnv("env_stage", "local")
	if env != string(EnvironmentStageProd) {
		// debug = false
	}

	requestID := req.RequestContext.RequestID
	if len(requestID) == 0 {
		requestID = "xxx"
	}

	// Add other fields to the ctx.Log
	log.Logrus = log.Logrus.WithField("request_id", requestID)
	appVersion := req.Headers["x-version"]
	if len(appVersion) != 0 {
		log.Logrus = log.Logrus.WithField("app_version", appVersion)
	}
	appPlatform := req.Headers["x-platform"]
	if len(appPlatform) != 0 {
		log.Logrus = log.Logrus.WithField("platform", appPlatform)
	}
	if len(req.QueryStringParameters) != 0 {
		log.Logrus = log.Logrus.WithField("query_params", req.QueryStringParameters)
	}
	if len(req.Body) != 0 {
		log.Logrus = log.Logrus.WithField("request_body", req.Body)
	}

	return &Context{
		Log: log.Logrus,
		Authorizer: &AuthorizerClaims{
			CognitoUserName: cognitoUsername,
			PhoneNumber:     phoneNumber,
		},
		DB: dbClient,
	}
}

// GetEnv returns environment variable value. If not present, returns default value
func GetEnv(envKey, defaultValue string) string {
	if len(os.Getenv(envKey)) != 0 {
		return os.Getenv(envKey)
	}
	return defaultValue
}

// Marshal a struct to json string
func Marshal(obj interface{}) string {
	res, err := json.Marshal(&obj)
	if err != nil {
		//TODO: think about an alternative
		panic(err)
	}

	return string(res)
}

// SuccessResponse returns a successful response with the header information
func SuccessResponse(statusCode int, body string) events.APIGatewayProxyResponse {
	if len(body) == 0 {
		// Return with empty body
		return events.APIGatewayProxyResponse{
			StatusCode: statusCode,
			Headers:    map[string]string{"Content-Type": "application/json"},
		}
	}

	// Return with body
	return events.APIGatewayProxyResponse{
		StatusCode: statusCode,
		Headers:    map[string]string{"Content-Type": "application/json"},
		Body:       body,
	}
}

// ErrorResponse returns a non-20X response with the header information
func ErrorResponse(err error, statusCode int, msg string) events.APIGatewayProxyResponse {
	// Marshal to error body to JSON
	e := APIError{
		Message: msg,
	}
	res := Marshal(&e)

	// // Capture in Sentry
	// if err != nil {
	// 	sentry.CaptureException(errors.Wrap(err, msg))
	// }

	return events.APIGatewayProxyResponse{
		StatusCode: statusCode,
		Headers:    map[string]string{"Content-Type": "application/json"},
		Body:       string(res),
	}
}
