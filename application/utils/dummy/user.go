package dummy

import (
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/google/uuid"
	"syreclabs.com/go/faker"
)

// ExistingUserRequestContext returns request context of a known User (already in Cognito + DB)
func ExistingUserRequestContext() events.APIGatewayProxyRequestContext {
	return events.APIGatewayProxyRequestContext{
		Authorizer: map[string]interface{}{
			"claims": map[string]interface{}{
				"cognito:username": TestUserCognitoUserName,
				"phone_number":     TestUserPhoneNumber,
				"given_name":       "Jane",
				"family_name":      "Doe",
			},
		},
	}
}

// NewRequestContext returns request context of an unknown user (or new user)
func NewRequestContext() events.APIGatewayProxyRequestContext {
	return events.APIGatewayProxyRequestContext{
		Authorizer: map[string]interface{}{
			"claims": map[string]interface{}{
				"cognito:username": uuid.New().String(),
				"phone_number":     faker.PhoneNumber().CellPhone(),
				"given_name":       faker.Name().FirstName(),
				"family_name":      faker.Name().LastName(),
				"email":            faker.Internet().Email(),
			},
		},
	}
}

// NewUserJSONBody for POST user endpoint
func NewUserJSONBody() string {
	return Compact(fmt.Sprintf(`
	{
		"first_name": "%s",
		"last_name": "%s",
		"email": "%s",
		"phone_number":"%s"
	}`, faker.Name().FirstName(), faker.Name().LastName(),
		faker.Internet().Email(), faker.PhoneNumber().CellPhone()))
}
