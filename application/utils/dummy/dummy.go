package dummy

import (
	"bytes"
	"database/sql"
	"encoding/json"

	"gitlab.com/jaincenterbc/application/utils"
	pg "gitlab.com/jaincenterbc/application/utils/db"
)

// Hard-coded test data
const (
	TestUserCognitoUserName = "92974c8f-c757-428f-b736-d677d273bfde"
	TestUserPhoneNumber     = "+17785490724"
)

// Compact will remove white space from json
func Compact(s string) string {
	buffer := new(bytes.Buffer)
	json.Compact(buffer, []byte(s))
	return buffer.String()
}

// CreateDbClient creates a DB Client
func CreateDbClient() *sql.DB {
	utils.ReadSecrets()
	db, err := pg.CreateConnection()
	if err != nil {
		panic(err)
	}
	return db
}

// CloseDbClientConnection closes db connect
func CloseDbClientConnection(db *sql.DB) {
	pg.CloseConnection(db)
}
