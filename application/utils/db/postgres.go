package db

import (
	"database/sql"
	"fmt"

	// postgres connection requirement
	_ "github.com/lib/pq" // postgres driver
	"gitlab.com/jaincenterbc/application/utils"
)

// CreateConnection connects to Postgres server
func CreateConnection() (*sql.DB, error) {
	var (
		EnvStage   = utils.GetEnv("env_stage", "local")
		DbHost     = utils.GetEnv("DB_HOST", "host.docker.internal")
		DbPort     = utils.GetEnv("DB_PORT", "5433")
		DbName     = utils.GetEnv("DB_NAME", "local_munim")
		DbUser     = utils.GetEnv("DB_USER", "postgres")
		DbPassword = utils.GetEnv("DB_PASSWORD", "Password1")
	)

	// TODO: sslMode preferably should be verify-full
	// For that, we need the cert for that.
	sslMode := "require"
	if EnvStage == "local" {
		sslMode = "disable"
	}

	// Connect to DB
	dbConnStr := fmt.Sprintf("host=%s port=%s dbname=%s user=%s password=%s sslmode=%s",
		DbHost, DbPort, DbName, DbUser, DbPassword, sslMode)

	db, err := sql.Open("postgres", dbConnStr)
	if err != nil {
		panic(err)
	}
	return db, nil
}

// CloseConnection closes the postgres connection
func CloseConnection(db *sql.DB) {
	db.Close()
}
