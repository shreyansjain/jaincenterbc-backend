package db_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	pg "gitlab.com/jaincenterbc/application/utils/db"
)

var _ = Describe("Postgres Test", func() {
	Context("Test CreateConnection()", func() {
		It("Create successful connection to db", func() {
			db, err := pg.CreateConnection()
			Expect(err).To(BeNil())
			Expect(db.Ping()).To(BeNil())
			pg.CloseConnection(db)
		})
	})
})
