package utils

import (
	"fmt"
	"os"
	"path/filepath"

	dot "github.com/joho/godotenv"
)

// ReadSecrets reads the .env file based on stage and make them accessible via Environment variable
func ReadSecrets() {
	envName := GetEnv("env_stage", "local")
	envFileName := ".env." + envName

	projectDirectory := GetEnv("PROJECT_DIR", "")
	envFilePath := projectDirectory + "/" + envFileName
	AbsEnvFilePath, err := filepath.Abs(envFilePath)
	if err != nil {
		panic(err)
	}

	err = dot.Load(AbsEnvFilePath)
	if err != nil {
		panic(err)
	}
}

// ReadSecretsFromPath reads the secret environment file from path
func ReadSecretsFromPath(path string) {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		panic(fmt.Errorf("EnvFile Path: %s does not exist", path))
	}
	absEnvFilePath, err := filepath.Abs(path)
	if err != nil {
		panic(err)
	}

	err = dot.Load(absEnvFilePath)
	if err != nil {
		panic(err)
	}
}
