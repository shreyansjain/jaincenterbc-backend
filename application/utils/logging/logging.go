package logging

import (
	"github.com/sirupsen/logrus"
)

// Logger ...
type Logger struct {
	Logrus *logrus.Entry
}

// initializeLogrus initialize the logrus client
func initializeLogrus() *logrus.Entry {
	logger := logrus.New()
	logger.SetFormatter(&logrus.JSONFormatter{})
	logger.SetLevel(logrus.DebugLevel)

	return logger.WithFields(logrus.Fields{})
}

// Init initializes log related clients
func Init() *Logger {
	// Initialize Logrus Client
	logrus := initializeLogrus()

	//TODO: Initialize sentry

	return &Logger{
		Logrus: logrus,
	}
}
