package utils_test

import (
	"os"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.com/jaincenterbc/application/utils"
)

var _ = Describe("Common", func() {
	Context("Test GetEnv()", func() {
		It("should read already existing environment variable", func() {
			os.Setenv("TEST_ENV_VARIABLE", "TEST_VALUE")
			envValue := utils.GetEnv("TEST_ENV_VARIABLE", "NOT_TEST_VALUE")
			Expect(envValue).To(Equal("TEST_VALUE"))
			Expect(envValue).NotTo(Equal("NOT_TEST_VALUE"))

			err := os.Unsetenv("TEST_ENV_VARIABLE")
			Expect(err).To(BeNil())
		})

		It("should return default environmet variable value", func() {
			envValue := utils.GetEnv("TEST_ENV_VARIABLE", "DEFAULT_VALUE")
			Expect(envValue).To(Equal("DEFAULT_VALUE"))
		})
	})
})
