package pkg_test

import (
	"testing"

	"github.com/aws/aws-lambda-go/events"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/jaincenterbc/application/utils"
	pg "gitlab.com/jaincenterbc/application/utils/db"
	"gitlab.com/jaincenterbc/application/utils/dummy"
	"gitlab.com/jaincenterbc/application/utils/logging"
)

var ctx *utils.Context

func TestUnit(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Pkg Suite")
}

var _ = BeforeSuite(func() {
	// For reading the secrets in environment variables
	utils.ReadSecrets()

	db, err := pg.CreateConnection()
	Expect(err).NotTo(HaveOccurred())
	Expect(db.Ping()).To(BeNil())

	request := events.APIGatewayProxyRequest{
		RequestContext: dummy.ExistingUserRequestContext(),
	}

	log := logging.Init()
	ctx = utils.InitContext(request, log, db)
})

var _ = AfterSuite(func() {
	pg.CloseConnection(ctx.DB)
})
