package pkg_test

import (
	"math/rand"

	"github.com/google/uuid"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"syreclabs.com/go/faker"

	u "gitlab.com/jaincenterbc/application/user"
	"gitlab.com/jaincenterbc/application/utils/dummy"
	"gitlab.com/jaincenterbc/pkg/user"
)

var _ = Describe("User", func() {
	Context("Test pgk/User", func() {
		It("should successfully UnMarshal() a json payload", func() {
			byteData := []byte(dummy.NewUserJSONBody())
			usr, err := user.UnMarshal(byteData)
			Expect(err).To(BeNil())
			Expect(usr.FirstName).NotTo(BeNil())
			Expect(usr.LastName).NotTo(BeNil())
			Expect(usr.Email).NotTo(BeNil())
			Expect(usr.CognitoUserName).NotTo(BeNil())
		})

		It("should successfully GetUser()", func() {
			// ctx already has dummy user
			usr, err := user.GetUser(ctx)
			Expect(err).To(BeNil())

			Expect(*usr.ID).Should(BeNumerically(">", 0))
			Expect(usr.FirstName).NotTo(BeNil())
			Expect(usr.LastName).NotTo(BeNil())
			Expect(usr.PhoneNumber).NotTo(BeNil())
			Expect(usr.CognitoUserName).NotTo(BeNil())
		})

		It("should successfully PostUser()", func() {
			clt := &u.User{
				CognitoUserName: uuid.New().String(),
				FirstName:       faker.Name().FirstName(),
				LastName:        faker.Name().LastName(),
				Email:           faker.Internet().Email(),
				PhoneNumber:     faker.PhoneNumber().CellPhone(),
			}

			err := user.PostUser(ctx, clt)
			Expect(err).To(BeNil())

			Expect(*clt.ID).Should(BeNumerically(">", 0))
			Expect(clt.FirstName).NotTo(BeNil())
			Expect(clt.LastName).NotTo(BeNil())
			Expect(clt.Email).NotTo(BeNil())
			Expect(clt.CognitoUserName).NotTo(BeNil())
		})

		It("should successfully PatchUser()", func() {
			updatedClient := &u.User{
				FirstName: faker.Name().FirstName(),
				LastName:  faker.Name().LastName(),
				Email:     faker.Internet().Email(),
			}

			// ctx already has dummy client user
			err := user.PatchUser(ctx, updatedClient)
			Expect(err).To(BeNil())
		})

		It("should successfully GetNotifications()", func() {
			notifs, err := user.GetNotifications(ctx)
			Expect(err).To(BeNil())

			if len(notifs) > 0 {
				random := 0
				if len(notifs) > 1 {
					random = rand.Intn(len(notifs) - 1)
				}

				Expect(*notifs[random].ID).Should(BeNumerically(">", 0))
				Expect(notifs[random].Title).NotTo(BeNil())
				Expect(notifs[random].Message).NotTo(BeNil())
				Expect(notifs[random].Created).NotTo(BeNil())
			}
		})
	})
})
