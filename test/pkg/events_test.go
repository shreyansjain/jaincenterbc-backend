package pkg_test

import (
	"math/rand"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.com/jaincenterbc/pkg/events"
)

var _ = Describe("Events", func() {
	Context("Test pgk/Events", func() {

		It("should successfully GetEvents()", func() {
			evnts, err := events.GetEvents(ctx)
			Expect(err).To(BeNil())

			if len(evnts) > 0 {
				random := 0
				if len(evnts) > 1 {
					random = rand.Intn(len(evnts) - 1)
				}

				Expect(*evnts[random].ID).Should(BeNumerically(">", 0))
				Expect(evnts[random].Title).NotTo(BeNil())
				Expect(evnts[random].Description).NotTo(BeNil())
				Expect(evnts[random].DateTime).NotTo(BeNil())
			}
		})
	})
})
