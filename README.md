# Jain Center of BC

Backend for Jain Center of BC Mobile App

## Deployment

We are using Gitlab CI/CD for deployments on `dev` an `prod` environment.

### Deployment to Production environment

Once the changes have been tested manually on the Dev apps, create a MR from `develop` to `main`.

Changes merged to `main` will be deployed to the Production environment.

## Secrets

We are using `openssl` for encrypting the secrets. The secrets are stored as `*.env` file which gets set in the container.
Gitlab uses `rydetoday/serverless-golang-1.16.5:sls-2.49.0` docker image to deploy the Lambda.

### Encrypt a secret

```bash
$ docker run -v $(pwd):/test -it rydetoday/serverless-golang-1.16.5:sls-2.49.0 bash
openssl enc -aes256 -salt -pbkdf2 -in .env.{environment} -out .env.{environment}.enc -pass pass:{Password}
```

The environment options are: `local` `test` `dev` `prod`.
Ask admin for `{Password}`

## Running Tests Locally (api-v2)

You can run unit and function tests locally. Following are the commands:

### Unit Test

```bash
go test -mod=vendor -run 'Unit' -v .
```

### Function Test

```bash
go test -mod=vendor -tags=function -run 'Function' -v .
```

## Dev setup

Instead of installing all dependency in your local, instead, use the Dockerfile in the project to develop locally.

### Using Dockerfile

Dockerfile in the package is the image that is being used by the Gitlab CI. We can use the same image for local development.

Alternatively, you can build your own using the following command:

```bash
docker build --rm --tag golang:1.16.5 .
```

To run the image:

```bash
docker run -v $PWD:/test -w /test --network="host" --env PROJECT_DIR=/test --env env_stage=local -it rydetoday/serverless-golang-1.16.5:sls-2.49.0 bash
```

-   Install GO project dependencies:

```bash
make install
```

## Extra Info

### DB Migration

Make sure to have the latest version of Docker (at least 18.03).

### Ginkgo Test

-   To create a `*_test.go` file:

```bash
ginkgo generate <SUBJECT>
```

-   To create test suite:

```bash
ginkgo bootstrap
```
