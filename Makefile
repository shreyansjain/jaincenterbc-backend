.PHONY: lint clean install

#########
# Basic #
#########

SRC=$(shell find . -type f -name '*.go' -not -path "./vendor/*")

## lint the files
lint:
	@echo "Starting linting..."
	golint -set_exit_status $(go list ./... | grep -v /vendor/)

fmt:
	@echo "Starting gofmt..."
	@gofmt -l -w $(SRC)

install:
	@echo "Installing the vendor via mod..."
	go mod vendor

 ## clean the binaries, serverless package and vendor
clean:
	@echo "Cleaning the temps..."
	rm -rf api-*/bin ./vendor api-*/.serverless

check-env:
ifndef SECRETS_GPG_PASSPHRASE
	$(error env:var SECRETS_GPG_PASSPHRASE is undefined)
endif

## decrypts the gpg file which has secrets
decrypt: check-env
	@echo "Decrypting secrets..."
	$(shell openssl enc -aes256 -pbkdf2 -d -in .env.$$env_stage.enc -out .env.$$env_stage -pass pass:$$SECRETS_GPG_PASSPHRASE)

build:
	@cicd/scripts/build.sh

deploy:
	@cicd/scripts/deploy.sh

################
# DB Migration #
################

db-migrate: decrypt
	@echo "Migrating DB..."
	go run -mod=vendor db/migrate.go


########
# Test #
########

coverage:
	GOLAGS=-mod=vendor go tool cover -func=./coverage.txt

unit-test:
	@echo "Executing unit tests..."
	go test -mod=vendor -run 'Unit' -covermode=atomic -coverprofile=./coverage.txt -v ./...

function-test:
	@echo "Executing Function tests..."
	go test -mod=vendor -tags=function -run 'Function' -covermode=atomic -coverprofile=./coverage.txt -v ./...

##########
# Gitlab #
##########

gitlab-unit-test: lint decrypt unit-test coverage

gitlab-integration-test: lint decrypt function-test coverage

gitlab-deploy: build decrypt deploy

