package events

import (
	"github.com/pkg/errors"
	e "gitlab.com/jaincenterbc/application/events"
	"gitlab.com/jaincenterbc/application/utils"
)

// GetEvents returns Events
func GetEvents(ctx *utils.Context) ([]*e.Event, error) {
	notifications, err := e.GetEvents(ctx.DB)
	if err != nil {
		ctx.Log.Errorf("Failed to GetEvents()", err)
		return nil, errors.WithStack(err)
	}

	return notifications, nil
}
