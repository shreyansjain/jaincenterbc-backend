package organization

import (
	"github.com/pkg/errors"
	o "gitlab.com/jaincenterbc/application/organization"
	"gitlab.com/jaincenterbc/application/utils"
)

// GetDirectors returns Board of Directors
func GetDirectors(ctx *utils.Context) ([]*o.BoardOfDirector, error) {
	directors, err := o.GetBoardDirectors(ctx.DB)
	if err != nil {
		ctx.Log.Errorf("Failed to GetBoardDirectors()", err)
		return nil, errors.WithStack(err)
	}

	return directors, nil
}
