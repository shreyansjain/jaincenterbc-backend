package user

import (
	"encoding/json"

	"github.com/pkg/errors"
	u "gitlab.com/jaincenterbc/application/user"
	"gitlab.com/jaincenterbc/application/utils"
)

// UnMarshal unmarshal the payload and returns the struct Object
func UnMarshal(payload []byte) (*u.User, error) {
	clt := new(u.User)
	err := json.Unmarshal(payload, &clt)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to unmarshal User")
	}
	return clt, nil
}

// GetUser returns User info
func GetUser(ctx *utils.Context) (*u.User, error) {
	clt := &u.User{CognitoUserName: ctx.Authorizer.CognitoUserName}
	err := clt.Get(ctx.DB)
	if err != nil {
		ctx.Log.Errorf("Failed to GetUser()", err)
		return nil, errors.WithStack(err)
	}

	return clt, nil
}

// PostUser creates a new User
func PostUser(ctx *utils.Context, clt *u.User) error {
	if len(clt.CognitoUserName) == 0 {
		clt.CognitoUserName = ctx.Authorizer.CognitoUserName
	}

	txn, err := ctx.DB.Begin()
	if err != nil {
		ctx.Log.Errorf("DB Begin Transaction Failed. Error: %s", err)
		return errors.Wrap(err, "DB operation failed")
	}

	err = clt.Create(txn)
	if err != nil {
		txn.Rollback()
		ctx.Log.Errorf("Failed to PostUser()", err)
		return errors.WithStack(err)
	}

	err = txn.Commit()
	if err != nil {
		ctx.Log.Errorf("DB Commit Transaction Failed. Error: %s", err)
		return errors.WithStack(err)
	}

	return nil
}

// PatchUser updates User info
func PatchUser(ctx *utils.Context, clt *u.User) error {
	clt.CognitoUserName = ctx.Authorizer.CognitoUserName

	txn, err := ctx.DB.Begin()
	if err != nil {
		ctx.Log.Errorf("DB Begin Transaction Failed. Error: %s", err)
		return errors.Wrap(err, "DB operation failed")
	}

	err = clt.Patch(txn)
	if err != nil {
		txn.Rollback()
		ctx.Log.Errorf("Failed to PatchUser()", err)
		return errors.WithStack(err)
	}

	err = txn.Commit()
	if err != nil {
		ctx.Log.Errorf("DB Commit Transaction Failed. Error: %s", err)
		return errors.WithStack(err)
	}

	return nil
}

// GetUserNotifications returns User Notifications
func GetNotifications(ctx *utils.Context) ([]*u.Notification, error) {
	clt := &u.User{CognitoUserName: ctx.Authorizer.CognitoUserName}
	notifications, err := clt.GetNotifications(ctx.DB)
	if err != nil {
		ctx.Log.Errorf("Failed to GetNotifications()", err)
		return nil, errors.WithStack(err)
	}

	return notifications, nil
}
