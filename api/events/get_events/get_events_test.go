package main_test

import (
	"encoding/json"
	"math/rand"
	"net/http"

	"github.com/aws/aws-lambda-go/events"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/jaincenterbc/api/events/get_events"
	e "gitlab.com/jaincenterbc/application/events"
	"gitlab.com/jaincenterbc/application/utils/dummy"
)

var _ = Describe("GetEvents", func() {
	var (
		response events.APIGatewayProxyResponse
		request  events.APIGatewayProxyRequest
		err      error
	)

	Context("Events", func() {
		BeforeEach(func() {
			request = events.APIGatewayProxyRequest{
				RequestContext: dummy.ExistingUserRequestContext(),
			}

			response, err = Handler(request)
			Expect(err).To(BeNil())
		})

		It("should be ok with statusCode: 200", func() {
			Expect(response.StatusCode).To(Equal(http.StatusOK))

			evts := make([]*e.Event, 0)
			err := json.Unmarshal([]byte(response.Body), &evts)
			Expect(err).To(BeNil())

			if len(evts) > 0 {
				random := 0
				if len(evts) > 1 {
					random = rand.Intn(len(evts) - 1)
				}

				Expect(*evts[random].ID).Should(BeNumerically(">", 0))
				Expect(evts[random].Title).NotTo(BeNil())
				Expect(evts[random].Description).NotTo(BeNil())
				Expect(evts[random].DateTime).NotTo(BeNil())
			}
		})
	})
})
