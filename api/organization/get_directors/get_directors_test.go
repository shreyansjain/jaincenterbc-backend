package main_test

import (
	"encoding/json"
	"math/rand"
	"net/http"

	"github.com/aws/aws-lambda-go/events"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/jaincenterbc/api/organization/get_directors"
	o "gitlab.com/jaincenterbc/application/organization"
	"gitlab.com/jaincenterbc/application/utils/dummy"
)

var _ = Describe("GetDirectors", func() {
	var (
		response events.APIGatewayProxyResponse
		request  events.APIGatewayProxyRequest
		err      error
	)

	Context("Org/Directors", func() {
		BeforeEach(func() {
			request = events.APIGatewayProxyRequest{
				RequestContext: dummy.ExistingUserRequestContext(),
			}

			response, err = Handler(request)
			Expect(err).To(BeNil())
		})

		It("should be ok with statusCode: 200", func() {
			Expect(response.StatusCode).To(Equal(http.StatusOK))

			directors := make([]*o.BoardOfDirector, 0)
			err := json.Unmarshal([]byte(response.Body), &directors)
			Expect(err).To(BeNil())

			if len(directors) > 0 {
				random := 0
				if len(directors) > 1 {
					random = rand.Intn(len(directors) - 1)
				}

				Expect(*directors[random].ID).Should(BeNumerically(">", 0))
				Expect(directors[random].FirstName).NotTo(BeNil())
				Expect(directors[random].LastName).NotTo(BeNil())
				Expect(directors[random].Email).NotTo(BeNil())
				Expect(directors[random].PhoneNumber).NotTo(BeNil())
				Expect(directors[random].Position).NotTo(BeNil())
			}
		})
	})
})
