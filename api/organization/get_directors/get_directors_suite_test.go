//+build function

package main_test

import (
	"testing"

	"gitlab.com/jaincenterbc/application/utils"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestFunction(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "GetDirectors Suite")
}

var _ = BeforeSuite(func() {
	// For reading the secrets in environment variables
	utils.ReadSecrets()
})
