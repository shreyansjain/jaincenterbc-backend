package main_test

import (
	"net/http"

	"github.com/aws/aws-lambda-go/events"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"syreclabs.com/go/faker"

	. "gitlab.com/jaincenterbc/api/user/patch_user"
	u "gitlab.com/jaincenterbc/application/user"
	"gitlab.com/jaincenterbc/application/utils"
	"gitlab.com/jaincenterbc/application/utils/dummy"
)

var _ = Describe("PatchUser", func() {
	var (
		response events.APIGatewayProxyResponse
		request  events.APIGatewayProxyRequest
		err      error
	)

	Context("Update User", func() {
		BeforeEach(func() {
			clt := &u.User{
				FirstName: faker.Name().FirstName(),
			}

			request = events.APIGatewayProxyRequest{
				Body:           dummy.Compact(utils.Marshal(clt)),
				RequestContext: dummy.ExistingUserRequestContext(),
			}

			response, err = Handler(request)
			Expect(err).To(BeNil())
		})

		It("should update an existing client with statusCode: 204", func() {
			Expect(response.StatusCode).To(Equal(http.StatusNoContent))
		})
	})
})
