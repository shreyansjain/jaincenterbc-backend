package main_test

import (
	"encoding/json"
	"net/http"

	"github.com/aws/aws-lambda-go/events"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"syreclabs.com/go/faker"

	. "gitlab.com/jaincenterbc/api/user/post_user"
	u "gitlab.com/jaincenterbc/application/user"
	"gitlab.com/jaincenterbc/application/utils"
	"gitlab.com/jaincenterbc/application/utils/dummy"
)

var _ = Describe("PostUser", func() {
	var (
		response events.APIGatewayProxyResponse
		request  events.APIGatewayProxyRequest
		err      error
	)

	Context("Create New User", func() {
		BeforeEach(func() {
			clt := &u.User{
				FirstName:   faker.Name().FirstName(),
				LastName:    faker.Name().LastName(),
				Email:       faker.Internet().Email(),
				PhoneNumber: faker.PhoneNumber().CellPhone(),
			}

			request = events.APIGatewayProxyRequest{
				Body:           dummy.Compact(utils.Marshal(clt)),
				RequestContext: dummy.NewRequestContext(),
			}

			response, err = Handler(request)
			Expect(err).To(BeNil())
		})

		It("should create a new User with statusCode: 201", func() {
			Expect(response.StatusCode).To(Equal(http.StatusCreated))

			respBody := u.User{}
			err = json.Unmarshal([]byte(response.Body), &respBody)
			Expect(err).To(BeNil())

			// Match the request body with response body
			Expect(*respBody.ID).Should(BeNumerically(">", 0))
			Expect(respBody.CognitoUserName).NotTo((BeNil()))
		})
	})
})
