package main_test

import (
	"encoding/json"
	"math/rand"
	"net/http"

	"github.com/aws/aws-lambda-go/events"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/jaincenterbc/api/user/get_notifications"
	u "gitlab.com/jaincenterbc/application/user"
	"gitlab.com/jaincenterbc/application/utils/dummy"
)

var _ = Describe("GetUserNotifications", func() {
	var (
		response events.APIGatewayProxyResponse
		request  events.APIGatewayProxyRequest
		err      error
	)

	Context("User Notifications", func() {
		BeforeEach(func() {
			request = events.APIGatewayProxyRequest{
				RequestContext: dummy.ExistingUserRequestContext(),
			}

			response, err = Handler(request)
			Expect(err).To(BeNil())
		})

		It("should be ok with statusCode: 200", func() {
			Expect(response.StatusCode).To(Equal(http.StatusOK))

			notifs := make([]*u.Notification, 0)
			err := json.Unmarshal([]byte(response.Body), &notifs)
			Expect(err).To(BeNil())

			if len(notifs) > 0 {
				random := 0
				if len(notifs) > 1 {
					random = rand.Intn(len(notifs) - 1)
				}

				Expect(*notifs[random].ID).Should(BeNumerically(">", 0))
				Expect(notifs[random].Title).NotTo(BeNil())
				Expect(notifs[random].Message).NotTo(BeNil())
				Expect(notifs[random].Created).NotTo(BeNil())
			}
		})
	})
})
