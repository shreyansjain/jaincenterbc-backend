package main

import (
	"net/http"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"gitlab.com/jaincenterbc/application/utils"
	pg "gitlab.com/jaincenterbc/application/utils/db"
	"gitlab.com/jaincenterbc/application/utils/logging"
	"gitlab.com/jaincenterbc/pkg/user"
)

// Handler Using AWS Lambda Proxy Request
func Handler(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// Initialize clients for Context
	log := logging.Init()
	db, err := pg.CreateConnection()
	if err != nil {
		log.Logrus.Errorf("Failed to CreateConnection(): %s", err)
		return utils.ErrorResponse(err, http.StatusInternalServerError, "Something went wrong"), nil
	}
	defer pg.CloseConnection(db)

	// Initialize Context
	ctx := utils.InitContext(req, log, db)

	clt, err := user.GetUser(ctx)
	if err != nil {
		return utils.ErrorResponse(err, http.StatusNotFound, "Failed to get user"), nil
	}

	res := utils.Marshal(clt)
	ctx.Log.Debug(res)
	return utils.SuccessResponse(http.StatusOK, res), nil
}

func main() {

	// Read the secret environment file in dev or prod stage
	envName := utils.GetEnv("env_stage", "local")
	if envName == string(utils.EnvironmentStageDev) || envName == string(utils.EnvironmentStageProd) {
		pwd, _ := os.Getwd()

		envFileName := ".env." + envName
		envFilePath := pwd + "/" + envFileName
		utils.ReadSecretsFromPath(envFilePath)
	}

	lambda.Start(Handler)
}
