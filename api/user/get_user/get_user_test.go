package main_test

import (
	"encoding/json"
	"net/http"

	"github.com/aws/aws-lambda-go/events"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/jaincenterbc/api/user/get_user"
	u "gitlab.com/jaincenterbc/application/user"
	"gitlab.com/jaincenterbc/application/utils/dummy"
)

var _ = Describe("GetUser", func() {
	var (
		response events.APIGatewayProxyResponse
		request  events.APIGatewayProxyRequest
		err      error
	)

	Context("User Exists", func() {
		BeforeEach(func() {
			request = events.APIGatewayProxyRequest{
				RequestContext: dummy.ExistingUserRequestContext(),
			}

			response, err = Handler(request)
			Expect(err).To(BeNil())
		})

		It("should be ok with statusCode: 200", func() {
			Expect(response.StatusCode).To(Equal(http.StatusOK))

			respBody := u.User{}
			err = json.Unmarshal([]byte(response.Body), &respBody)
			Expect(err).To(BeNil())

			Expect(*respBody.ID).Should(BeNumerically(">", 0))
			Expect(respBody.FirstName).ShouldNot(BeEmpty())
			Expect(respBody.LastName).ShouldNot(BeEmpty())
			Expect(respBody.PhoneNumber).ShouldNot(BeEmpty())
		})
	})

	Context("No Client Exists with statusCode: 404", func() {
		BeforeEach(func() {
			request = events.APIGatewayProxyRequest{
				RequestContext: dummy.NewRequestContext(),
			}

			response, err = Handler(request)
			Expect(err).To(BeNil())
		})

		It("should fail for no client found", func() {
			Expect(response.StatusCode).To(Equal(http.StatusNotFound))
			Expect(response.Body).NotTo(BeNil())
		})
	})
})
